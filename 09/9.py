'''
This is probably the messiest solution so far so please skip this
'''
from typing import List, Set, Tuple

Grid = List[List[int]]

def parse_file() -> Grid:
    with open("input", "r") as file:
        grid = []
        for line in file:
            row = []
            for elem in line.strip():
                row.append(int(elem))
            grid.append(row)

    return grid


def is_lowpoint(y: int, x: int, grid: Grid) -> bool:
    cols = len(grid)
    rows = len(grid[0])

    current = grid[y][x]
    # top
    if y != 0 and grid[y - 1][x] <= current:
        return False
    # left
    if x != 0 and grid[y][x - 1] <= current:
        return False
    # right
    if x < cols - 1 and grid[y][x + 1] <= current:
        return False
    # bottom
    if y < rows - 1 and grid[y + 1][x] <= current:
        return False
    return True


def risk_levels() -> int:
    grid = parse_file()

    total_risk = 0
    cols = len(grid)
    rows = len(grid[0])
    for y in range(cols):
        for x in range(rows):
            if is_lowpoint(y, x, grid):
                total_risk += grid[y][x] + 1

    return total_risk


def basins() -> int:
    grid = parse_file()

    largest_basins = [0, 0, 0]
    cols = len(grid)
    rows = len(grid[0])
    for y in range(cols):
        for x in range(rows):
            if not is_lowpoint(y, x, grid):
                continue

            basin_size = get_basin_size(y, x, grid, set())

            largest_basins.append(basin_size)
            largest_basins.remove(min(largest_basins))

    return largest_basins[0] * largest_basins[1] * largest_basins[2]


def get_basin_size(y: int, x: int, grid: Grid,
                        already_checked: Set[Tuple[int, int]]) -> int:
    cols = len(grid)
    rows = len(grid[0])
    current = grid[y][x]
    if (x, y) in already_checked:
        return 0
    already_checked.add((x, y))
    total = 1
    # top
    if y != 0 and 9 > grid[y - 1][x] > current:
        total += get_basin_size(y - 1, x, grid, already_checked)
    # left
    if x != 0 and 9 > grid[y][x - 1] > current:
        total += get_basin_size(y, x - 1, grid, already_checked)
    # right
    if x < cols - 1 and 9 > grid[y][x + 1] > current:
        total += get_basin_size(y, x + 1, grid, already_checked)
    # bottom
    if y < rows - 1 and 9 > grid[y + 1][x] > current:
        total += get_basin_size(y + 1, x, grid, already_checked)
    return total


if __name__ == "__main__":
    print(f'Part 1: {risk_levels()}')
    print(f'Part 2: {basins()}')
