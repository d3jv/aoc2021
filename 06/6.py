from typing import Dict, Tuple, List
from collections import defaultdict


def parse_file() -> Dict[int, int]:
    with open("input", "r") as file:
        strings = file.readline().split(",")
        fishes = defaultdict(int)
        for i in range(len(strings)):
            fishes[i] = int(strings[i])
        return fishes


def simulate_fish(days: int):
    fishes = parse_file()

    real_counter = len(fishes)
    counter = len(fishes)
    # (x, y) where x is number of fish and y their timer
    new_fishes: Dict[int, Tuple[int, int]] = defaultdict(int)
    for i in range(days):
        fresh_fish = 0
        for fish in range(counter):
            if fishes[fish] == 0:
                fishes[fish] = 6
                fresh_fish += 1 
            else:
                fishes[fish] -= 1
        j = 0
        for j in new_fishes:
            if new_fishes[j][1] == 0:
                fresh_fish += new_fishes[j][0]
                new_fishes[j] = (new_fishes[j][0], 6)
            else:
                new_fishes[j] = (new_fishes[j][0], new_fishes[j][1] - 1)
        if fresh_fish > 0:
            real_counter += fresh_fish
            new_fishes[j + 1] = (fresh_fish, 8)

    return real_counter


if __name__ == "__main__":
    print(f'Part 1: {simulate_fish(80)}')
    print(f'Part 2: {simulate_fish(256)}')
