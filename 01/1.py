def count_increases(path: str) -> int:
    file = open(path, "r")
    lines = file.readlines()
    file.close()

    nums = []
    for line in lines:
        nums.append(int(line[:-1]))

    # for parts 1 and 2 respectively
    count1 = 0
    count2 = 0
    for i in range(len(nums)):
        # part 1
        if i != 0 and nums[i] > nums[i - 1]:
            count1 += 1

        #part 2
        if i >= len(nums) - 3:
            continue

        a = nums[i] + nums[i + 1] + nums[i + 2]
        b = nums[i + 1] + nums[i + 2] + nums[i + 3]

        if b > a:
            count2 += 1
    
    return count1, count2


if __name__ == "__main__":
    p1, p2 = count_increases("input.txt")
    print(f'Part 1: {p1}')
    print(f'Part 2: {p2}')
