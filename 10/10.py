from typing import Dict, Tuple


def corrupted() -> Tuple[int]:
    autocomp_points: Dict[str. int] = {")": 1, "]": 2, "}": 3, ">": 4}
    syntax_points: Dict[str, int] = {")": 3, "]": 57, "}": 1197, ">": 25137}
    brackets: Dict[str, str] = {"(": ")", "[": "]", "{": "}", "<": ">"}

    with open("input", "r") as f:
        syntax_score = 0
        autocomp_scores = []
        for line in f:
            expected = ""
            is_corrupted = False
            for char in line.strip():
                if char in {"(", "[", "{", "<"}:
                    expected = brackets[char] + expected
                elif char != expected[0]:
                    syntax_score += syntax_points[char]
                    is_corrupted = True
                    break
                else:
                    expected = expected[1:]
            if not is_corrupted:
                score = 0
                for char in expected:
                    score *= 5
                    score += autocomp_points[char]
                autocomp_scores.append(score)

    return syntax_score, sorted(autocomp_scores)[len(autocomp_scores) // 2]


if __name__ == "__main__":
    p1, p2 = corrupted()
    print(f'Part 1: {p1}')
    print(f'Part 2: {p2}')

