from typing import List, Optional


def parse_file() -> List[int]:
    with open("input", "r") as file:
        strings = file.readline().strip().split(",")

    numbers = []
    for elem in strings:
        numbers.append(int(elem))

    return numbers


def calculate_price(crabs: List[int]) -> int:
    max_crab = max(crabs)
    min_crab = min(crabs)

    min_total_fuel: Optional[int] = None
    for i in range(min_crab, max_crab + 1):
        total_fuel = 0
        for crab in crabs:
            total_fuel += abs(crab - i)
        if min_total_fuel is None or total_fuel < min_total_fuel:
            min_total_fuel = total_fuel

    return min_total_fuel


def calculate_price2(crabs: List[int]) -> int:
    max_crab = max(crabs)
    min_crab = min(crabs)

    min_total_fuel: Optional[int] = None
    for i in range(min_crab, max_crab + 1):
        total_fuel = 0
        for crab in crabs:
            for step in range(1, abs(crab - i) + 1):
                total_fuel += step
        if min_total_fuel is None or total_fuel < min_total_fuel:
            min_total_fuel = total_fuel

    return min_total_fuel


if __name__ == "__main__":
    print(f'Part 1: {calculate_price(parse_file())}')
    print("(Next part takes like 5 minutes but it's correct)")
    print(f'Part 2: {calculate_price2(parse_file())}')

